/**
 * @author jtsheke @ UQAM  -- 2017-01-31
 * une solution du lab3 INF1256 -- Il y a moyen de programmer une autre solution
 * Ex: solution utilisant try, solution n'utilisant pas switch, etc.
 */
package lab3;

import java.util.*;
public class ConversionDistance {
	public static final double MILE_TO_KM = 1.609344; // 1 mile = 1.609344 km
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int OPT_KM_TO_MILE = 1;//option 1
		final int OPT_MILE_TO_KM = 2; //option 2
		
      System.out.println("Bienvenu sur le programme de converion d'échelles");
      System.out.println("Ce programme fait la comversion mile vers km et vice-versa");
      
      Scanner clavier = new Scanner(System.in);
      System.out.println(" Veuillez faire un choix svp");
      System.out.format("%d : Convertir Km en miles %n", OPT_KM_TO_MILE);
      System.out.format("%d : Convertir miles en km %n", OPT_MILE_TO_KM);
      if(!clavier.hasNextInt()){//si l'utilisateur ne saisit pas un nombre entier
    	  System.out.println("Erreur: Il faut taper le nombre entier correspondant");
    	  System.out.println("Exécutez le programme à nouveau");
      }
      else{
    	  int choixUtilisateur = clavier.nextInt();
    	  switch(choixUtilisateur){
    	  case OPT_KM_TO_MILE:{
    		  System.out.println("Veuillez entrer le nombre de km svp ");
    		  if(!clavier.hasNextDouble()){
    			  System.out.println("Erreur: Il faut saisir un nombre réel");
    		  }else{
    			  double nombreKm = clavier.nextDouble();
    			  double nombreMiles = (nombreKm / MILE_TO_KM);
    			  System.out.print(nombreKm);
    			  System.out.format(" Km  = %.3f Miles %n",nombreMiles);
    		  }
    		  
    	     }
    	     break;
    	  case OPT_MILE_TO_KM:{
    		  System.out.println("Veuillez entrer le nombre de miles svp ");
    		  if(!clavier.hasNextDouble()){
    			  System.out.println("Erreur: Il faut saisir un nombre réel");
    		  }else{
    			  double nombreMiles = clavier.nextDouble();
    			  double nombreKm = (nombreMiles * MILE_TO_KM);
    			  System.out.print(nombreMiles);
    			  System.out.format(" Miles  = %.3f Km %n",nombreKm);
    		  }
    	       }
    	      break;
    	   default: System.out.format("Ce choix %d n'est pas valide %n",choixUtilisateur);   
    	  }
      }
      clavier.close();
	}

}
